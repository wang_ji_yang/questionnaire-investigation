'use scrict'

let fs = require('fs')

let path = require('path')

let router = require('koa-router')();

function searchControllers(dir) {
    let tmpPath = fs.readdirSync(dir);

    let resultFiles = tmpPath.filter((item) => {
        return item.endsWith('.js') && item !== "index.js"
    })
    return resultFiles
}
// 注册给定的路由数组下的所有定义的路由
function registeForEachFile(files, currentPath) {
    files.forEach(item => {
        let tmpPath = path.join(currentPath, item);

        let routerObj = require(tmpPath)

        for (let key in routerObj) {
            let arrValue = routerObj[key];
            let type = routerObj[key][0]
            if (type === 'get') {
                if(arrValue.length === 2){
                    let fn = arrValue[1];
                    router.get(key,fn)
                }else{
                    let auth=arrValue[1];
                    let fn=arrValue[2];
                    router.get(key,auth,fn);
                }
            } else {
                // 为了判断传入的数组里面有没有包含auth，没有正按正常处理；有的话，需要auth传入作为第二参数
                if(arrValue.length===2){
                    let fn=arrValue[1];
                    router.post(key, fn);
                }else{
                    let auth=arrValue[1];
                    let fn=arrValue[2];
                    router.post(key,auth,fn);
                }
            }
        }
    })
}
// 3、将本文件当作控制器目录的入口，在app.js当中作为中间注册

module.exports = function (dir) {

    let defaultDir = dir || 'controllers'

    //当前路径
    let root = path.resolve('.')
    //获取控制器绝对路径
    let resultControllerPath = path.join(root, defaultDir);

    let files = searchControllers(resultControllerPath);

    // 注册定义的路由
    registeForEachFile(files, resultControllerPath);

    //返回待在app执行的注册函数
    return router.routes();
}
