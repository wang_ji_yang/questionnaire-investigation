let {defineModel,DataTypes}=require('../dt/index');


let model=defineModel('questionnaireName',{
    fromQuestionnaireId:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    
    questionnaire:DataTypes.STRING(80)
});

module.exports=model;