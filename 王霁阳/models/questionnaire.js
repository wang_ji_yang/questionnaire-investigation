let {defineModel,DataTypes}=require('../dt/index');


let model=defineModel('questionnaire',{
    fromUserId:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    questionnaire:{
        type:DataTypes.STRING(80),
        allowNull:false
    },
});

module.exports=model;