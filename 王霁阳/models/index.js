"use strcit"

let fs = require("fs")

let {sequelize}=require("../dt/index")

//遍历文件名

function searchFiles() {
    let files = fs.readdirSync(__dirname);
    return files.filter(item=>{
        return item.endsWith(".js") && item!=='index.js'
    })
}

function registerModel(files) {
    let obj = {};
    
    files.forEach(element => {
        let key = element.substring(0,element.length-3)
        obj[key]=require(__dirname+"/"+element)
    });

    return obj
}

let files = searchFiles()

let obj = registerModel(files)

obj.sync = async()=>{
    if(process.env==='product'){
        console.log('当前是生产环境，不能强制修改数据表或者删除数据表');
    }else{
        return sequelize.sync({force:true})
    }
}

module.exports=obj