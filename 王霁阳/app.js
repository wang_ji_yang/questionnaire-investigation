'use strict';

// 引入需要的各种模块
let Koa = require('koa');
let bodyParser = require('koa-bodyparser');
let controller = require('./controllers');
let templating=require('./templating');
let statics=require("koa-static");
// let User=require('./models/User');
// let Message=require('./models/Message');
let session = require("./session")
let models=require('./models');


// 初始化一个服务器实例
let app = new Koa();
app.keys = ['some secret hurr']
app.use(session(app))
app.use(statics(__dirname+'/static'));
// 注册中间件 注意先注册post请求的解释器，后注册路由
app.use(bodyParser());

app.use(templating);

app.use(controller());

// 程序运行的时候，初始化数据表
(async ()=>{
    //await models.sync();
})();



// 定义端口，设置监听
let port = 4000;
app.listen(port);

// 打印服务器信息
console.log(`http://127.0.0.1:${port}`);