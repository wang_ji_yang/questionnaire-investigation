module.exports=async (ctx,next) =>{
    let userId = ctx.session.userId;
    let path = ctx.path;
    if(userId){
        if(path === "/login"){
            ctx.redirect('/');
        }else{
            await next();
        }
    }else{
        if(path === '/login'){
            await next();
        }else{
            ctx.redirect('/login')
        }
    }
}