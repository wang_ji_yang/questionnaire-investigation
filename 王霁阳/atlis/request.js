function request(url,data,success_fn,error_fn){
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        success: function (res) {
            success_fn(res);
          
        },
        error: function (msg) {
            error_fn(msg);
        }
    })
}
