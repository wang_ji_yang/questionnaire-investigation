'use scrict'
//暴露模块
let { User, questionnaireScore, Comment, questionnaire, questionnaireName } = require('../models')
let auth = require('../atlis/auth');
const { username } = require('../dt/config');
const { render } = require('nunjucks');
let index_fn = async (ctx, next) => {



    let username = ctx.session.userName;
    let msgs = JSON.parse(JSON.stringify(await questionnaire.findAll({
        order: [
            ["id", "desc"]
        ]
    })));
    //时间戳转换成时间
    msgs.forEach(row => {
        let createdAt = row.createdAt * 1;
        let date = new Date(createdAt);
        var retData = {
            nYear: date.getFullYear(),
            nMonth: date.getMonth() + 1,
            nDay: date.getDate(),
            nHour: date.getHours(),
            nMinutes: date.getMinutes(),
            nSeconds: date.getSeconds()
        };
        row.createdAt = retData.nYear + '年' + retData.nMonth + '月' + retData.nDay + "日  " + retData.nHour + '时' + retData.nMinutes + "分"
    })


    let userIds = [];

    msgs.forEach(row => {
        userIds.push(row.fromUserId);
    })


    let userAll = await User.findAll({
        where: {
            id: userIds
        }
    })

    msgs.forEach(row => {
        let currentUser = userAll.filter(user => {
            return row.fromUserId === user.id;
        })[0];
        row.comUserName = currentUser.username;
    })

    let obj = {
        username: username,
        msgs: msgs

    }
    ctx.render('index.html', obj)
}
let login_fn = async (ctx, next) => {
    ctx.render('login.html')
}
let register_fn = async (ctx, next) => {
    ctx.render('register.html')
}
let loginDone_fn = async (ctx, next) => {
    let username = ctx.request.body.username || '';
    let password = ctx.request.body.password || '';
    let res;
    let u1 = await User.findOne({
        where: {
            username: username,
            password: password
        }
    }).then((row) => {
        let user = JSON.stringify(row);
        let u2 = JSON.parse(user);
        console.log(user);
        console.log(u2);

        if (user !== 'null') {
            ctx.session.userId = row.id;
            ctx.session.userName = row.username;
            res = { code: 200, msg: '登录成功' };
        } else {
            res = { code: 1000, msg: '用户名或密码错误，请重试' };
        }
    });
    ctx.body = res;
}

let registerDone_fn = async (ctx, next) => {
    let username = ctx.request.body.username || '';
    let password = ctx.request.body.password || '';
    let passwordtwo = ctx.request.body.passwordtwo || '';

    console.log(username);
    console.log(password);
    console.log(passwordtwo);

    if (username.length > 0 && password.length > 0 && passwordtwo.length > 0 && password === passwordtwo) {
        let userDemo = await User.findOne({ where: { username: username } });
        console.log(JSON.stringify(userDemo));
        let res = '';

        // 如果有找到同名的用户，则返回提示消息，并且不创建用户；否则创建用户，并返回提示消息
        if (userDemo) {
            // msg='当前用户名已经注册，请确认后重试';
            res = { code: 1000, msg: '当前用户名已经注册，请确认后重试' };
        } else {
            let ul = User.create({
                username: username,
                password: password
            });
            res = { code: 200, msg: '注册成功' };
        }

        ctx.body = JSON.stringify(res);
    }

}
let logout_fn = async (ctx, next) => {
    ctx.session = null;
    ctx.body = {
        code: 200,
    }
}
//管理员登录界面跳转
let adminLogin_fn = async (ctx, next) => {
    ctx.render("adminLogin.html")
}

let evaluate_fn = async (ctx, next) => {
    let points=0,average=0,ints=0;
    let msgId = ctx.query.msgId || null;
    //根据msgId找到所有的问题
    let ListquestionnaireName = JSON.parse(JSON.stringify(await questionnaireName.findAll({
        where: {
            fromQuestionnaireId: msgId
        }
    })))


    //尝试：定义一个数组，把ListquestionnaireName里Id的值全部提取出来
    let list = [];

    ListquestionnaireName.forEach(row=>{
        list.push(row.id)
    })

    let ListquestionnaireScore = JSON.parse(JSON.stringify(await questionnaireScore.findAll({
        where: {
            fromQuestionnaireNameId: list
        }
    })))
    //拿到用户名Id
    let listUserId = []
    ListquestionnaireScore.forEach(row=>{
        listUserId.push(row.fromUserId)
    })
    //找到打分的用户Id
    let userAll = JSON.parse(JSON.stringify(await User.findAll({
        where: {
            id: listUserId
        }
    })))
    //把找到的用户表里的名字对应到评分表
    ListquestionnaireScore.forEach(row=>{
        let users = userAll.filter(user=>{
            return row.fromUserId == user.id
        })[0];
        row.comUserName=users.username;
    })
    //现在的问题1：把用户所有评分，合并成一条数据传给前面，
    ListquestionnaireName.forEach(item=>{
        let a = ListquestionnaireScore.filter(ScoreId=>{
            if(item.id == ScoreId.fromQuestionnaireNameId){
                return ScoreId.fromQuestionnaireNameId==item.id
            }
        })
        item.score = a
    })
    
    //尝试，把所有的评分，按照用户区分开,前面已经找到有多少个用户了，所以语句是有多少个人点评，先创建多少个空数组
    //scoress
    let scoress = [];
     userAll.forEach(item=>{
         //修改名字的属性名，改成分数，这样直接在前端循环出来
         scoress.push([JSON.parse(JSON.stringify(item).replace(/username/g,"score"))])
     })
console.log(ListquestionnaireName);

    ListquestionnaireName.forEach(item=>{
        scoress.forEach(row=>{
            for(let a in item.score){
                if(row[0].id===item.score[a].fromUserId){
                   
                    row.push(item.score[a])
                }
            }
            
        })
    })
    console.log(scoress);
    //狠，人不狠站不稳
    let twoListquestionnaireName = JSON.parse(JSON.stringify(ListquestionnaireName))
  
    ListquestionnaireName.unshift({
        questionnaire: '用户'
    })
    ListquestionnaireName.push({
        questionnaire: '总分'
    });
    ListquestionnaireName.push({
        questionnaire: '平均分'
    });
    //明明创建了一个名叫twoListquestionnaireName的数组，但是只修改ListquestionnaireName的话twoListquestionnaireName也会一起跟着改变
    console.log(ListquestionnaireName);
    console.log(twoListquestionnaireName);
    //使用循环把分数相加，第一重循环来循环有多少人评分，第二重循环来计算，计算完后在第一重循环添加
    for(let r = 0 ;r<scoress.length;r++){
        for(let bits=1 ; bits<scoress[r].length;bits++){
            points=points+scoress[r][bits].score*1
            ints+=1
        }
        scoress[r].push({
            score:points
        })
        scoress[r].push({
            score:points/ints
        })
        points=0
        ints=0
    }
   
    //把找到的题目提交给前端
    let obj = {
        username: username,
        ListquestionnaireName: ListquestionnaireName,
        scoress:scoress,
        twoListquestionnaireName:twoListquestionnaireName
    }

    ctx.render("evaluate.html", obj)
}
//添加问卷分数
let userinscore_fn = async (ctx,next)=>{
    let userId = ctx.session.userId;
    let userCoresValue = ctx.request.body;
    //创建一个布尔值，默认是true如果分数大于10或者小于0那么布尔值改变，就不添加数组
    let boot = true
    for(let r in userCoresValue){
        //r是fromQuestionnaireNameId  userCoresValue[r]是score 这个循环判断数据库是否已经创建了
        if(userCoresValue[r]>10 || userCoresValue[r]<0||userCoresValue[r]==""){
            boot=false;
            break;
        }
        let ul=await questionnaireScore.findOne({ where: { fromUserId: userId,fromQuestionnaireNameId:  r} }); 
        if(ul!==null){
            //创建了直接返回空值，这样不会污染数据库
            userCoresValue=null
            break;
        }
    }
    if(boot){
        for(let r in userCoresValue){
            //r是fromQuestionnaireNameId  userCoresValue[r]是score
                   questionnaireScore.create({
                    fromUserId: userId,
                    score: userCoresValue[r],
                    fromQuestionnaireNameId:r
                });
        }
    }
    if(userCoresValue==null){
        ctx.body={code:1000,msg:'你已经发布过了，别发布了'};
    }else if(!boot){
        ctx.body={code:1100,msg:'请输入0-10的数字'};
    }else{
        ctx.body={code:200,msg:'发表成功'};
    }
    
}
let Release_fn = async (ctx, next) => {
    ctx.render("Release.html")
}
let createRelease_fn = async (ctx,next)=>{
    //获取用户Id，来判断是谁祖册的
    
    let userId = ctx.session.userId

    //获取传来的详情信息
    let userCoresValue = ctx.request.body;

    //先添加主表

    //找到详情和名字

    let remarks = userCoresValue.remarks;
    let name = userCoresValue.title

    //先判断这个用户有没有添加过同样的表

    let ul = await questionnaire.findOne({
        where:{
            fromUserId:userId,
            questionnaire:name,
        }
    })
    if(ul  == null){

        
        let a=await  questionnaire.create({
            fromUserId:userId,
            questionnaire:name,
            remarks:remarks
        })
        
        console.log(a.id);
        //现在添加名字表，首先找到刚刚添加的Id
       
        //把对象里的remarks和title删了
        delete userCoresValue.remarks;
        delete userCoresValue.title;
        //这样，这个对象里面就都是名字了,然后循环添加
        for(let r in userCoresValue){
            //r是fromQuestionnaireNameId  userCoresValue[r]是score
            questionnaireName.create({
                    fromQuestionnaireId: a.id,
                    questionnaire: userCoresValue[r],
                });
        }
        ctx.body={code:200,msg:'完美'};
    }else{
        ctx.body={code:1000,msg:'你已经发布过同样的问卷了，别发布了'};
    }

}
module.exports = {
    '/': ['get', auth, index_fn],
    '/login': ['get', auth, login_fn],
    '/register': ['get', register_fn],
    '/loginDone': ['post', loginDone_fn],
    '/registerDone': ['post', registerDone_fn],
    '/logout': ["post", logout_fn],
    '/adminLogin': ['get', adminLogin_fn],
    '/evaluate': ['get', evaluate_fn],
    '/Release': ['get', Release_fn],
    '/userinscore':['post',userinscore_fn],
    '/createRelease':['post',createRelease_fn]
}