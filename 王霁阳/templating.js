'use strict'

let nunjucks = require('nunjucks')

//创建env的实例模块

function createEnv(path,opts) {
    path = path || "views";
    opts = opts || {};
    let envOptions={
        autoescape :opts.autoescape || true,
        throwOnUndefined :opts.throwOnUndefined || false,
        trimBlocks : opts.trimBlocks || false,
        lstripBlocks :opts.lstripBlocks || false,
        watch :opts.watch||true,
        noCache :opts.noCache || true
    }
    let env = nunjucks.configure(path,envOptions)

    return env
}

//暴露模块
module.exports =async (ctx,next)=>{
    let env = createEnv();

    ctx.render = function (views,model) {
        ctx.body=env.render(views,model);
    }
    await next();
}
