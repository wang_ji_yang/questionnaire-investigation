let {defineModel,DataTypes}=require('../dt/index');


let model=defineModel('questionnaireScore',{
    fromQuestionnaireNameId:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    fromUserId:{
        type:DataTypes.BIGINT,
        allowNull:false
    },
    score:{
        type:DataTypes.BIGINT(10),
        allowNull:false
    },
    questionnaire:DataTypes.STRING(80)
});

module.exports=model;