"use strict"

let {database,username,password,host,dialect}=require('./config')

let {Sequelize,DataTypes}=require("sequelize")

let sequelize = new Sequelize(database,username,password,{
    host:host,
    dialect:dialect
})

function defineModel(name,attrs) {
    let poop = {}

    //主键autoIncrement是自增
    poop.id={
        type:DataTypes.BIGINT,
        primaryKey:true,
        autoIncrement:true

    }

    //让attrs对象的值赋给poop临时对象，这样就可以修改创建的时间了
    for(let key in attrs){
        poop[key]=attrs[key]
    }
    // 创建时间、修改时间、版本、备注
    // 创建时间
    poop.createdAt={
        type:DataTypes.BIGINT,
        allowNull:false
    }
    // 更新时间
    poop.updatedAt={
        type:DataTypes.BIGINT,
        allowNull:false
    }
    // 版本
    poop.version={
        type:DataTypes.BIGINT,
        allowNull:false
    }
    // 备注
    poop.remarks={
        type:DataTypes.STRING(800),
        allowNull:true
    }

    // 真正利用Sequelize的实例定义一个模型的方式

    let obj =sequelize.define(name,poop,{
        //强制表名同步
        tableName:name,

        //不自动创建修改时间和创建时间属性
        timestamps:false,
        // 勾子对象
        hooks:{
             // 生命周期勾子,意思是在，对将要插入或更新到数据库的数据进行验证之前，执行的方法（这里就是在验证数据之前，给createdAt、updatedAt添加指定的值）
             beforeValidate:function(obj) {
                 let date = Date.now()
                if(obj.isNewRecord){
                    obj.createdAt=date;
                    obj.updatedAt=date;
                    obj.version=0
                }else{
                    obj.updatedAt=date
                    obj.version+=1
                }
             }
        }
        
    })
    return obj;
}

let obj = {
    sequelize,DataTypes,defineModel
}
module.exports=obj